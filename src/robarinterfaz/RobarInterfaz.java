/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package robarinterfaz;

/**
 *
 * @author Álvaro Lillo Igualada <@alilloig>
 */
public class RobarInterfaz {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        LadronDeCasas ladronCasas = new LadronDeCasas();
        LadronDeTiendas ladronTiendas = new LadronDeTiendas();
        Politico politico = new Politico();
        System.out.println("Para entrar a robar en una casa hay que...");
        ladronCasas.entrarARobar();
        System.out.println("Para entrar a robar en una tienda hay que...");
        ladronTiendas.entrarARobar();
        System.out.println("Para poder robar de verdad hay que...");
        ladronTiendas.entrarARobar();
    }
    
}
